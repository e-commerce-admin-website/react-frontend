// Libs
import React from 'react'

// Components 
import LoginForm from '../components/LoginForm'


import { login } from '../actions/authen'

const handleSubmit = (values, dispatch) => {

    const { email, password } = values

    dispatch(login(email, password))
}

const LoginPage = props => {

    return (
        <div>
            <LoginForm
                onSubmit={handleSubmit}
                title='Please login to continue'
                isLoginForm={true}
            />
        </div>
    )
}

export default LoginPage