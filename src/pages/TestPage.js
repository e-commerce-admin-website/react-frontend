// Libs
import React from 'react'

// Components 

import ProductList from '../components/ProductList'

const TestPage = props => {

    return (
        <div>
            <ProductList />
        </div>
    )
}

export default TestPage