// Libs
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

// Components
import ProductForm from '../components/ProductForm'

// Actions
import { findById, updateProduct } from '../actions/products'

const ProductsEditPage = props => {

    const { match } = props
    const { id } = match.params

    const product = useSelector(state => state.products.productsDetail.data)

    const dispatch = useDispatch()

    useEffect(() => {

        dispatch(findById(id))

    }, [dispatch, id])

    const handleSubmitProduct = (id, values, dispatch) => {

        const coverImage = values.products_image.imageUrl
            ? values.products_image.imageUrl
            : values.products_image

        const payload = {
            ...values,
            products_id: id,
            products_image: coverImage
        }

        dispatch(updateProduct(payload))

    }

    return (
        <ProductForm
            title='Update product post data'
            initialValues={product}
            imageUrl={product.products_image}
            onSubmit={(values, dispatch) => {
                handleSubmitProduct(id, values, dispatch)
            }}
        />
    )

}

export default ProductsEditPage