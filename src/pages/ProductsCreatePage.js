// Libs
import React from 'react'

// Components 
import ProductForm from '../components/ProductForm'

// Actions
import { createProduct } from '../actions/products'

const ProductsCreatePage = props => {

    const handleCreateProduct = (values, dispatch) => {

        const payload = {
            ...values,
            products_create_date: new Date(),
            products_image: values.products_image.imageUrl
        }

        dispatch(createProduct(payload))
    }

    return (
        <ProductForm
            title='Create new product post'
            onSubmit={handleCreateProduct}
        />
    )

}

export default ProductsCreatePage