// Libs
import { combineReducers } from 'redux'

// reducers
import productsListReducer from './productsListReducer'
import productsDetailReducer from './productsDetailReducer'

export default combineReducers({
    productsList: productsListReducer,
    productsDetail: productsDetailReducer
})
