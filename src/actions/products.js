// Utils
// import { db } from '../utilities/firebase'
import { createActionSet } from '../utilities'

// Libs  
import { push } from 'connected-react-router'
import axios from 'axios'
import jwt from 'jsonwebtoken'

const SECRET = 'MY_SECRET_KEY'

// Set Variable For Collection Products
//const productCol = db.collection('products')

// Set Type Action For Authentication
const FETCH_PRODUCTS = createActionSet('FETCH_PRODUCTS')
const FETCH_PRODUCT_DETAIL = createActionSet('FETCH_PRODUCT_DETAIL')
const CREATE_PRODUCT = createActionSet('CREATE_PRODUCT')
const UPDATE_PRODUCT = createActionSet('UPDATE_PRODUCT')
const DELETE_PRODUCT = createActionSet('DELETE_PRODUCT')

export const findAll = () => async dispatch => {

    // dispatch({
    //     type: FETCH_PRODUCTS.PENDING
    // })

    // try {

    //     const products = await productCol.orderBy('product_create_date', 'desc').get()

    //     const payload = products.docs.map(doc => {
    //         return {
    //             id: doc.id,
    //             ...doc.data()
    //         }
    //     })

    //     dispatch({
    //         type: FETCH_PRODUCTS.SUCCESS,
    //         payload
    //     })

    // }
    // catch (error) {

    //     dispatch({
    //         type: FETCH_PRODUCTS.FAILED,
    //         error
    //     })

    // }

    try {

        const dateNow = new Date()

        const payload = {
            email: 'tester',
            name: 'tester',
            iat: dateNow.getTime() // มาจากคำว่า issued at time (สร้างเมื่อ)
        }

        const authOptions = {
            method: 'GET',
            url: 'https://innate-might-268310.appspot.com/products/',
            headers: {
                'Authorization': jwt.sign(payload, SECRET)
            }
        }

        const products = await axios(authOptions)

        setTimeout(

            dispatch({
                type: FETCH_PRODUCTS.SUCCESS,
                payload: products.data.data
            })
            , 1000)

    }
    catch (error) {

        dispatch({
            type: FETCH_PRODUCTS.FAILED,
            error
        })

    }

}

export const findById = id => async dispatch => {

    // dispatch({
    //     type: FETCH_PRODUCT_DETAIL.PENDING
    // })

    // try {

    //     const product = await productCol.doc(id).get()

    //     dispatch({
    //         type: FETCH_PRODUCT_DETAIL.SUCCESS,
    //         payload: product.data()
    //     })

    // }
    // catch (error) {

    //     dispatch({
    //         type: FETCH_PRODUCT_DETAIL.FAILED,
    //         error
    //     })

    // }

    dispatch({
        type: FETCH_PRODUCT_DETAIL.PENDING
    })

    try {

        const dateNow = new Date()

        const payload = {
            email: 'tester',
            name: 'tester',
            iat: dateNow.getTime() // มาจากคำว่า issued at time (สร้างเมื่อ)
        }

        const authOptions = {
            method: 'GET',
            url: `https://innate-might-268310.appspot.com/products/${id}`,
            headers: {
                'Authorization': jwt.sign(payload, SECRET)
            }
        }


        const product = await axios(authOptions)

        dispatch({
            type: FETCH_PRODUCT_DETAIL.SUCCESS,
            payload: product.data.data
        })

    }
    catch (error) {

        dispatch({
            type: FETCH_PRODUCT_DETAIL.FAILED,
            error
        })

    }
}

export const createProduct = body => async dispatch => {


    dispatch({
        type: CREATE_PRODUCT.PENDING
    })

    // try {

    //     const response = await productCol.add(data)

    //     dispatch({
    //         type: CREATE_PRODUCT.SUCCESS,
    //         payload: response
    //     })

    //     dispatch(push('/products'))

    // }
    // catch (error) {

    //     dispatch({
    //         type: CREATE_PRODUCT.FAILED,
    //         error
    //     })

    // }


    try {

        const authOptions = {
            method: 'POST',
            url: 'https://innate-might-268310.appspot.com/products/create',
            data: body,
            headers: {
                'Authorization': localStorage.getItem('currentUser')
            }
        }

        const response = await axios(authOptions)

        dispatch({
            type: CREATE_PRODUCT.SUCCESS,
            payload: response
        })

        dispatch(push('/products'))

    }
    catch (error) {

        dispatch({
            type: CREATE_PRODUCT.FAILED,
            error
        })

    }

}

export const updateProduct = (body) => async dispatch => {

    // dispatch({
    //     type: UPDATE_PRODUCT.PENDING
    // })

    // try {

    //     await productCol.doc(id).update(data)

    //     dispatch({
    //         type: UPDATE_PRODUCT.SUCCESS
    //     })

    //     dispatch(push('/products'))

    // } catch (error) {

    //     dispatch({
    //         type: UPDATE_PRODUCT.FAILED,
    //         error
    //     })

    // }

    dispatch({
        type: UPDATE_PRODUCT.PENDING
    })

    try {

        const authOptions = {
            method: 'PUT',
            url: 'https://innate-might-268310.appspot.com/products/edit',
            data: body,
            headers: {
                'Authorization': localStorage.getItem('currentUser')
            }
        }

        await axios(authOptions)

        dispatch({
            type: UPDATE_PRODUCT.SUCCESS
        })

        dispatch(push('/products'))


    } catch (error) {

        dispatch({
            type: UPDATE_PRODUCT.FAILED,
            error
        })

    }
}


export const deleteProduct = id => async dispatch => {

    // dispatch({
    //     type: DELETE_PRODUCT.PENDING
    // })

    // try {

    //     await productCol.doc(id).delete()

    //     dispatch({
    //         type: DELETE_PRODUCT.SUCCESS
    //     })

    //     dispatch(push('/products'))

    // }
    // catch (error) {

    //     dispatch({
    //         type: DELETE_PRODUCT.FAILED,
    //         error
    //     })

    // }

    dispatch({
        type: DELETE_PRODUCT.PENDING
    })

    try {

        const authOptions = {
            method: 'DELETE',
            url: `https://innate-might-268310.appspot.com/products/${id}`,
            headers: {
                'Authorization': localStorage.getItem('currentUser')
            }
        }

        await axios(authOptions)

        dispatch({
            type: DELETE_PRODUCT.SUCCESS
        })

        dispatch(push('/products'))

    }
    catch (error) {

        dispatch({
            type: DELETE_PRODUCT.FAILED,
            error
        })

    }

}
