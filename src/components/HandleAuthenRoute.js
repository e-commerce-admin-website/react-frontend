// Lib
import React, { Component } from 'react'
import { connect } from 'react-redux'

const HandleAuthenRoute = ComposedComponent => {

    class Authentication extends Component {

        componentWillMount() {
            !this.props.isAuthenticated && this.props.history.push('/login')
        }

        componentWillUpdate(nextProps) {
            !nextProps.isAuthenticated && this.props.history.push('/login')
        }

        render() {
            return <ComposedComponent {...this.props} />
        }
    }

    const mapStateToProps = state => ({
        isAuthenticated:
            state.authen.isAuthenticated || localStorage.getItem('currentUser') !== null
    })

    return connect(mapStateToProps)(Authentication)
}

export default HandleAuthenRoute