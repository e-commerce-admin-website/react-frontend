// Libs
import React from 'react'
import { Link } from 'react-router-dom'

// Components 
import ProductItem from './ProductItem'

// Material UI
import {
    Button,
    Grid
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

// Set CSS
const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(2, 2, 4)
    }
}))

const ProductList = props => {

    const classes = useStyles()

    return (
        <div className='album py-5'>
            <div className='container'>
                <div className='row'>
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        className={classes.button}
                    >
                        <Link to='/products/create' style={{ textDecoration: 'none', color: '#FFFFFF' }} >
                            CREATE PRODUCT
                        </Link>
                    </Button>
                    <Grid container spacing={4}>
                        <ProductItem />
                    </Grid>
                </div>
            </div>
        </div>
    )

}

export default ProductList