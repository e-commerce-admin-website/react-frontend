// Libs
import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

// Actions
import {
    findAll,
    deleteProduct
} from '../../actions/products'

import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Hidden from '@material-ui/core/Hidden'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
    },
    cardDetails: {
        marginLeft: theme.spacing(3),
        flex: 1,
    },
    cardMedia: {
        height: 210,
        width: 345,
    },
    editButtom: {
        color: '#FFFFFF'
    },
    deleteButton: {
        marginLeft: theme.spacing(2)
    }
}))

const ProductItem = props => {

    const classes = useStyles()

    const dispatch = useDispatch()

    let products = useSelector(state => state.products.productsList)

    useEffect(() => {

        dispatch(findAll())

    }, [dispatch])

    const onHandleDelete = (id) => {

        const isConfirm = window.confirm('Are you sure to delete?')

        if (isConfirm) {
            dispatch(deleteProduct(id))
        }

    }

    return (
        <React.Fragment>
            {
                products.data.length > 0 && products.data.map((item, key) => {
                    return (
                        < Grid item xs={12} key={key} >
                            <Card className={classes.card}>
                                <Hidden xsDown>
                                    <CardMedia className={classes.cardMedia} image={item.products_image} title={item.products_name} />
                                </Hidden>
                                <div className={classes.cardDetails}>
                                    <CardContent>
                                        <Typography component="h2" variant="h5">
                                            {item.products_name}
                                        </Typography>
                                        <Typography variant="subtitle1" color='secondary'>
                                            PRICE : {item.products_price}
                                        </Typography>
                                        <Typography variant="subtitle1" color='primary'>
                                            {item.products_category}
                                        </Typography>
                                        <Typography variant="subtitle1" paragraph>
                                            {item.products_description}
                                        </Typography>

                                        <Link to={`/products/edit/${item.products_id}`} style={{ textDecoration: 'none', color: '#FFFFFF' }}>
                                            <Button
                                                variant='contained'
                                                className={classes.editButtom}
                                                startIcon={<EditIcon />}
                                            >
                                                EDIT
                                            </Button>
                                        </Link>

                                        <Button
                                            variant='contained'
                                            color='secondary'
                                            className={classes.deleteButton}
                                            startIcon={<DeleteIcon />}
                                            onClick={() => { onHandleDelete(item.products_id) }}
                                        >
                                            DELETE
                                        </Button>

                                    </CardContent>
                                </div>
                            </Card>
                        </Grid >
                    )
                })
            }
        </React.Fragment >
    )
}

export default ProductItem