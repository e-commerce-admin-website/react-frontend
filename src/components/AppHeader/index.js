// Libs
import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

// images
import logo from './logo-final.png'

// Actions 
import { fetchCurrentUser, logout } from '../../actions/authen'

// Meterial - UI
import { makeStyles } from '@material-ui/core/styles'
import {
    AppBar,
    Toolbar,
    Button
} from '@material-ui/core'

// Config CSS
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    button: {
        marginRight: theme.spacing(2)
    },
    logo: {
        flexGrow: 1
    }
}))

const AppHeader = props => {

    const classes = useStyles()

    const dispatch = useDispatch()

    const handleLogout = () => {
        dispatch(logout())
    }

    const isAuthenticated = useSelector(state => state.authen.isAuthenticated)

    useEffect(() => {

        dispatch(fetchCurrentUser())

    }, [dispatch])

    return (
        <div className={classes.root}>
            <AppBar position='static'>
                <div className='container'>
                    <Toolbar>

                        <div className={classes.logo}>
                            <img src={logo} alt='logo' width='120' height='40' />
                        </div>

                        <Button color='inherit' className={classes.button}>
                            <Link to='/' style={{ textDecoration: 'none', color: '#FFFFFF' }}>
                                Home
                            </Link>
                        </Button>

                        {
                            isAuthenticated ?
                                (
                                    <React.Fragment>
                                        <Button color='inherit' className={classes.button}>
                                            <Link to='/products' style={{ textDecoration: 'none', color: '#FFFFFF' }}>
                                                PRODUCTS
                                            </Link>
                                        </Button>

                                        <Button
                                            variant='contained'
                                            className={classes.button}
                                            onClick={handleLogout}
                                            style={{ color: 'red' }}
                                        >
                                            LOGOUT
                                        </Button>

                                    </React.Fragment>
                                ) :
                                (
                                    <Link to='/login' style={{ textDecoration: 'none' }}>
                                        <Button
                                            variant='contained'
                                            className={classes.button}
                                        >
                                            Login
                                        </Button>
                                    </Link>
                                )
                        }
                    </Toolbar>
                </div>
            </AppBar>
        </div>
    )
}

export default AppHeader