// Libs
import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

// Actions
import { findAll } from '../actions/products'

// Material UI
import { makeStyles } from '@material-ui/core/styles'
import {
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    Typography
} from '@material-ui/core'

// Set CSS
const useStyles = makeStyles(theme => ({
    cardRoot: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    paper: {
        height: 140,
        width: 100,
    }
}))

const HomeProductList = props => {

    const classes = useStyles()

    const dispatch = useDispatch()

    let products = useSelector(state => state.products.productsList)

    useEffect(() => {

        dispatch(findAll())

    }, [dispatch])

    return (
        <div className='album py-5'>
            <div className='container'>
                <div className='row'>
                    {
                        products.data.length > 0 && products.data.map((item, key) => {

                            const linkToDetail = `/products/${item.products_id}`

                            return (
                                <div key={key} className='col-md-4 mb-4'>
                                    <Link to={linkToDetail} style={{ textDecoration: 'none' }}>
                                        < Card className={classes.cardRoot} >
                                            <CardHeader
                                                title={item.products_name}
                                                subheader={item.products_category}
                                            />
                                            <CardMedia
                                                className={classes.media}
                                                image={item.products_image}
                                                title='Paella dish'
                                            />
                                            <CardContent>
                                                <Typography variant='h6' color='secondary' component='p'>
                                                    PRICE : {item.products_price}
                                                </Typography>
                                                <Typography variant='body2' color='textSecondary' component='p'>
                                                    {item.products_description}
                                                </Typography>
                                            </CardContent>
                                        </Card >
                                    </Link>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </div >
    )
}

export default HomeProductList