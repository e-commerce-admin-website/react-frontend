// Libs
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

// Config Can Change
const firebaseConfig = {
    apiKey: "AIzaSyC2AH1bSG25Us61IrH1COvZJclqoGetfsA",
    authDomain: "e-commerce-admin-website.firebaseapp.com",
    databaseURL: "https://e-commerce-admin-website.firebaseio.com",
    projectId: "e-commerce-admin-website",
    storageBucket: "e-commerce-admin-website.appspot.com",
    messagingSenderId: "169988743301",
    appId: "1:169988743301:web:c3aeb1c0ca37c4ce9a49ec"
}

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

db.settings({
    timestampsInSnapshots: true
})

export {
    db,
    auth,
    storage
}